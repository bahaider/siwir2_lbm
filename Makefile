SHELL        = /bin/bash
CXX          = g++

TARGET       = lbm

SOURCES      = $(wildcard src/*.cpp)
OBJECTS      = $(SOURCES:.cpp=.o)
 
FLAGS        = -march=native -mtune=native -std=c++11 -Wall -Wextra -O3 -DNDEBUG
DEBUGFLAGS   = -g -DDEBUG
RELEASEFLAGS = -DNDEBUG
INCPATH      = 
LIBPATH      = 
LIBS         = 
 
all: $(TARGET)

%.o: %.cpp
	$(CXX) -c $(FLAGS) $(INCPATH) $< -o $@
 
$(TARGET): $(OBJECTS)
	$(CXX) $(FLAGS) $(INCPATH) $(LIBPATH) $(LIBS) $(OBJECTS) -o $(TARGET)

clean:
	rm -f $(TARGET) $(OBJECTS)

.PHONY : all clean
