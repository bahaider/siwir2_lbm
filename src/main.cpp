#include	<cstdlib>
#include	<iostream>
#include	<string>

#include	"Timer.h"
#include	"LBMD2Q9.h"
#include	"inputReader.h"

int main( int argc, char **argv ) {
	///******************************************************
	///********************** INPUT *************************
	///******************************************************

	if ( argc != 2 ) {
		std::cout << "invalid number of arguments!" << std::endl;
		std::cout << "call: lbm params.dat" << std::endl;
		return EXIT_FAILURE;
	}

	InputData data = InputReader::readFromTextFile(argv[1]);
	//InputData data = InputReader::readFromPGMFile(argv[1]);

	LBMD2Q9 lbm( data.size_x, data.size_y, data.omega, data.obstacles );

	///******************************************************
	///********************** CALCULATION *******************
	///******************************************************

	siwir::Timer	timer;

	for ( unsigned int i = 0; i < (unsigned int) data.timesteps; i += data.vtk_step ){
		lbm.run( data.vtk_step );
		lbm.writeVTKToFile(data.vtk_file);
	}
	
	std::cout << "MLUPS: " << lbm.getNumberOfCells() * lbm.getNumberOfSimulationSteps() / timer.elapsed() * 1e-6 << std::endl;
	std::cout << "time (s): " << timer.elapsed() << std::endl;

    ///******************************************************
    ///********************** OUTPUT ************************
    ///******************************************************

    return EXIT_SUCCESS;
};
