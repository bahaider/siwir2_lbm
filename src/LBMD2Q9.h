#pragma once

#include	<vector>
#include	<string>

class LBMD2Q9{
public:
	static const unsigned int PADDING = 12;
	
	static const unsigned int DIRECTIONS = 9;
	enum Direction {C = 0, N = 1, S = 2, E = 3, W = 4, NE = 5, NW = 6, SE = 7, SW = 8};

	static const double weight[DIRECTIONS];

	LBMD2Q9(const unsigned int sizeS, const unsigned int sizeT, const double omega, int obstacles[]);
	LBMD2Q9(const LBMD2Q9& lbm) = delete;
	LBMD2Q9& operator=(const LBMD2Q9& x) = delete;
	~LBMD2Q9();

	inline
	double getrho( const unsigned int s, const unsigned int t ) const;
	inline
	double getux( const unsigned int s, const unsigned int t ) const;
	inline
	double getuy( const unsigned int s, const unsigned int t ) const;

	void addNoSlipObstacle( const unsigned int s, const unsigned int t );

	void run( const unsigned int steps );

	void writeVTKToFile(std::string vtkPath) const;
	void writePythonToFile() const;
        
        inline
        unsigned int getNumberOfCells() const;
        
        inline
        unsigned int getNumberOfSimulationSteps() const;

private:
	unsigned int sizeS_;
	unsigned int sizeT_;
	double omega_;
	
	unsigned int ld_;
	unsigned int fieldSize_;
	
	std::vector<double*> src_;
	std::vector<double*> dst_;
	double* rho_;
	double* ux_;
	double* uy_;

	double totrho_;

	std::vector<unsigned int> noSlipObstacles_;

	unsigned int simulationStep_ = 0;
	
	inline
	unsigned int getLinIndex(const unsigned int s, const unsigned int t) const;

	void init( const double rho, const double ux, const double uy );
	void boundaryHandling();
	void streamAndCollide();
};

unsigned int LBMD2Q9::getLinIndex(const unsigned int s, const unsigned int t) const{
	return t * ld_ + s;
}

double LBMD2Q9::getrho( const unsigned int s, const unsigned int t ) const {
	return rho_[getLinIndex( s, t )];
}

double LBMD2Q9::getux( const unsigned int s, const unsigned int t ) const {
	return ux_[getLinIndex( s, t )];
}

double LBMD2Q9::getuy( const unsigned int s, const unsigned int t ) const {
	return uy_[getLinIndex( s, t )];
}

unsigned int LBMD2Q9::getNumberOfCells() const{
    return (sizeS_ - 2) * (sizeT_ - 2);
}

unsigned int LBMD2Q9::getNumberOfSimulationSteps() const{
    return simulationStep_;
}
